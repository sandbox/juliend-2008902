<?php

/**
 * @file
 */


/**
 * The Controller for product entities
 */
class CommerceMarketplacesFeedEntityAPIController extends EntityAPIController {
  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a default CommerceMarketplaceFeed entity.
   *
   * @param array $values
   *   An array of defaults values to add to the object construction.
   * @return object
   */
  public function create(array $values = array()) {
    global $user;

    $values += array(
      'is_new' => TRUE,
      'id' => NULL,
      'type' => '',
      'name' => '',
      'title' => '',
      'source_product_types' => array(),
      'target_product_types' => array(),
      'fields_mapping' => array(),
      'frequency' =>'',
      'uid' => $user->uid,
      'status' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );

    return parent::create($values);
  }


  /**
   * Saves a feed.
   *
   *
   * @param object $feed
   *   The full object to save.
   * @param DatabaseTransaction $db_transaction
   *   An optional transaction object.
   *
   * @return bool|int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   *
   * @throws Exception
   */
  public function save($feed, DatabaseTransaction $db_transaction = NULL) {
    if (!isset($db_transaction)) {
      $db_transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      global $user;

      // Determine if we will be inserting a new transaction.
      $feed->is_new = empty($feed->id);

      // Set the timestamp fields.
      if (empty($feed->created)) {
        $feed->created = REQUEST_TIME;
      }
      else {
        // Otherwise if the payment transaction is not new but comes from an
        // entity_create() or similar function call that initializes the created
        // timestamp to an empty string, unset it to prevent destroying existing
        // data in that property on update.
        if ($feed->created === '') {
          unset($feed->created);
        }
      }

      $feed->changed = REQUEST_TIME;

      return parent::save($feed, $db_transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $db_transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }

}


class CommerceMarketplacesFeed extends Entity {

  public function __construct(array $values = array()) {
    parent::__construct($values, 'commerce_marketplaces_feed');
  }

  public function getId() {
    return $this->id;
  }

  public function getTitle() {
    return $this->title;
  }

  public function getType() {
    return $this->type;
  }

  public function getStatus() {
    return $this->status;
  }

/*
 *
 */
  public function getMarketplace() {
    if (!isset($this->marketplace)) {
      $this->marketplace = commerce_marketplaces_marketplace_load($this->type);
    }
    return $this->marketplace;
  }


  // Declare the mapping between Priceminister fields form and Commerce marketplace
  // Marketplace => Commerce Marketplace
  /*
   *
   */
  public function fieldsMapping() {
    $marketplace = $this->getMarketplace();
    return $marketplace->fieldsMapping();
  }


  /*
   * Get the list of fiels available for this product from the marketplace.
   *
   * Return all available fields if $required = NULL.
   * Return required fields if $required = TRUE.
   * Rerurn non-required fields if $required = FALSE.
   */
  public function getTargetProductTypeFields($condition = NULL) {
    if (!isset($this->target_product_type_fields)) {
      $marketplace = $this->getMarketplace();
      $this->target_product_type_fields = $marketplace->getProductTypeFields($this->target_product_types);
    }

    if ($this->target_product_type_fields) {
      $field_mapping = $this->fieldsMapping();
      if (is_array($condition)) {
        if ($condition['required'] == TRUE) {
          foreach ($this->target_product_type_fields as $key => $field) {
            if ($field[$field_mapping['required']] == TRUE) {
              $fields[$key] = $field;
            }
          }
        }
        elseif ($condition['required'] == FALSE) {
          foreach ($this->target_product_type_fields as $key => $field) {
            if ($field[$field_mapping['required']] == FALSE) {
              $fields[$key] = $field;
            }
          }
        }
        return $fields;
      }
      return $this->target_product_type_fields;
    }
    return array();
  }


  /*
   * Get the list of fiels available for this product from the marketplace.
   */
  public function getTargetProductTypeMappedFields($mapped = TRUE) {

    $mapped_fields = array();
    foreach($this->fields_mapping as $key => $mapping) {
      if (isset($mapping['target']['field'])) {
        $mapped_fields[$key] = $mapping['target']['field'];
      }
      elseif (isset($mapping['target']['value'])) {
        $mapped_fields[$key] = $mapping['target']['value'];
      }
    }

    if ($mapped == TRUE) {
      return $mapped_fields;
    }
    else {
      $product_type_fields = $this->getTargetProductTypeFields();
      return array_diff_key($product_type_fields, $mapped_fields);
    }
  }

  /*
   *
   */
  public function getTargetProductTypeField($field_name) {
    $field = $this->getTargetProductTypeFields();
    if (!empty($field) && isset($field[$field_name])) {
      return $field[$field_name];
    }
  }

  /*
   *
   */
  public function getTargetProductTypeFieldProperty($field, $property) {
    $field_mapping = $this->fieldsMapping();
    if ($field_mapping[$property] && $field[$field_mapping[$property]]) {
      return $field[$field_mapping[$property]];
    }
  }


  // Get the list of fiels available for this product from the marketplace.
  public function getSourceProductTypeFields($field = NULL) {

    if (!isset($this->target_product_fields)) {
      $this->source_product_type_fields = commerce_marketplaces_feed_get_source_fields_mapping($this->source_product_types);
    }
    if ($field) {
      return $this->target_product_type_fields[$field];
    }
    return $this->target_product_type_fields;
  }

  public function generateExport() {
    $query = new EntityFieldQuery();

    $query->entityCondition('entity_type', 'commerce_product')
      ->entityCondition('bundle', reset($this->source_product_types))
      ->propertyCondition('status', 1)
      ->addMetaData('account', user_load(1)); // Run the query as user 1.

    $result = $query->execute();
    dsm($result);

    $products = array();
    if (isset($result['commerce_product'])) {
      $products_ids  = array_keys($result['commerce_product']);
      $products = entity_load('commerce_product', $products_ids);
    }
    dsm($products, '$products');

    $exports = array();
    foreach($products as $product) {
      foreach($this->fields_mapping as $field_name => $field) {
        if (key($field['source']) == 'field') {
          $commerce_product_wrapper = entity_metadata_wrapper('commerce_product', $product);
          $value = $commerce_product_wrapper->{$field['source']['field']}->value();
        }
        else {
          $value = $field['source']['value'];
        }

        $export[$field_name] = array(
          'value' => $value,
          'config' => $field,
        );
      }
      $exports[] = $export;
    }
    dsm(__CLASS__ . '-' . __FUNCTION__);
    return $exports;
  }
}