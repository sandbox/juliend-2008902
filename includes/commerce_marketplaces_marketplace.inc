<?php

/**
 * @file
 */


/**
 * Controle classe for marketplaces.
 */
class CommerceMarketplacesMarketplaceController extends EntityBundlePluginEntityControllerExportable {

  public function __construct($entityType) {
    parent::__construct($entityType);
  }

  /**
   * Create a default Commerce Marketpalces Marketplace entity.
   *
   * @param array $values
   *   An array of defaults values to add to the object construction.
   * @return object
   */
  public function create(array $values = array()) {
    $values += array(
      'is_new' => TRUE,
      'id' => NULL,
      'type' => '',
      'name' => '',
      'title' => '',
      'config' => array(),
      'status' => '',
      'created' => REQUEST_TIME,
      'changed' => REQUEST_TIME,
    );
    return parent::create($values);
  }

  /**
   * Saves a marketplace.
   *
   *
   * @param object $marketplace
   *   The full object to save.
   * @param DatabaseTransaction $db_transaction
   *   An optional transaction object.
   *
   * @return bool|int
   *   SAVED_NEW or SAVED_UPDATED depending on the operation performed.
   *
   * @throws Exception
   */
  public function save($marketplace, DatabaseTransaction $db_transaction = NULL) {
    if (!isset($db_transaction)) {
      $db_transaction = db_transaction();
      $started_transaction = TRUE;
    }

    try {
      global $user;

      // Determine if we will be inserting a new transaction.
      $marketplace->is_new = empty($marketplace->id);

      // Set the timestamp fields.
      if (empty($marketplace->created)) {
        $marketplace->created = REQUEST_TIME;
      }
      else {
        // Otherwise if the payment transaction is not new but comes from an
        // entity_create() or similar function call that initializes the created
        // timestamp to an empty string, unset it to prevent destroying existing
        // data in that property on update.
        if ($marketplace->created === '') {
          unset($marketplace->created);
        }
      }

      $marketplace->changed = REQUEST_TIME;

      return parent::save($marketplace, $db_transaction);
    }
    catch (Exception $e) {
      if (!empty($started_transaction)) {
        $db_transaction->rollback();
        watchdog_exception($this->entityType, $e);
      }
      throw $e;
    }
  }
}


/**
 * @file
 * The class for the Commerce Marketplace entity.
 */
abstract class CommerceMarketplacesMarketplace extends Entity implements CommerceMarketplacesMarketplaceInterface {

  /**
   * Constructor.
   *
   * @see Entity::__construct()
   */
  public function __construct(array $values) {
    parent::__construct($values, 'commerce_marketplaces_marketplace');
    $this->plugin = commerce_marketplaces_get_marketplace_plugins($this->type);
  }

  public function isConfigurable() {
    return $this->plugin['configurable'] === TRUE ? TRUE : FALSE;
  }

  public function getId() {
    return $this->id;
  }

  public function getName() {
    return $this->name;
  }

  public function getType() {
    return $this->type;
  }

  public function getTitle() {
    return check_plain($this->title);
  }

  public function getDescription() {
    return check_plain($this->plugin['description']);
  }

  public function getStatus() {
    return $this->status ? t('Enable'): t('Disable');
  }

  public function getConfig($key = NULL) {
    if ($key) {
      return $this->config[$key];
    }
    return $this->config;
  }

  public function setConfig($key, $value) {
    return $this->config[$key] = $value;
  }

  /**
   * Return the default configuration.
   *
   * @return array
   *   Array where keys are the variable names of the configuration elements and
   *   values are their default values.
   */
  public function configDefaults() {
    return array();
  }

  /**
   * Return a configuration form for this object.
   *
   * The keys of the configuration
   * form must match the keys of the array returned by configDefaults().
   *
   * @param $form
   *   A $form array() variable.
   * @param $form_state
   *   The $form array() variable
   * @param $marketplace
   *   A marketplace object.
   *
   * @return array
   *   FormAPI style form definition.
   */
  public function configForm($form, &$form_state, $marketplace) {
    return array();
  }

  /**
   * Validation handler for configForm().
   *
   * Set errors with form_set_error().
   *
   * @param $form
   *   A $form array() variable.
   * @param $form_state
   *   The $form array() variable
   * @return array
   *
   * TODO Give only the $form_state['values'] instead of $form and $form_state.
   * param $values
   *   An array that contains the values entered by the user through configForm.
   */
  public function configFormValidate($form, &$form_state) {
    return array();
  }

  /**
   * Submission handler for configForm().
   *
   * @param $form
   *   A $form array() variable.
   * @param $form_state
   *   The $form array() variable
   *
   * TODO Give only the $form_state['values'] instead of $form and $form_state.
   * param $values
   *   An array that contains the values entered by the user through configForm.
   */
  public function configFormSubmit($form, &$form_state) {
    $this->save();
    drupal_set_message(t('Your changes have been saved.'));
  }

  public function fieldsMapping() {}


  public function generateExport(CommerceMarketplacesFeed $feed) {
    return $feed->generateExport();
  }

}

interface CommerceMarketplacesMarketplaceInterface {

  public function getProductTypes();

  public function getProductTypeFields($product_type);

}

/**
 * Manages exceptions.
 */
class CommerceMarketplacesMarketplaceBroken extends Exception {};