<?php

/**
 *
 */
function commerce_marketplaces_feeds_overview() {

  $header = array(
    t('Id'),
    t('Title'),
    t('Marketplace'),
    t('Status'),
    t('Operations'),
  );

  $feeds = entity_load_multiple_by_name('commerce_marketplaces_feed');

  $rows = $content = array();
  foreach ($feeds as $feed) {
    // Operations.
    $operations = array(
      l(t('Edit'), 'admin/commerce/config/marketplaces/feed/' . $feed->getId() . '/edit'),
      l(t('Mapping'), 'admin/commerce/config/marketplaces/feed/' . $feed->getId() . '/mapping'),
      l(t('Delete'), 'admin/commerce/config/marketplaces/feed/' . $feed->getId() . '/delete'),
    );

    $rows[] = array(
      'data' => array(
        'id' => $feed->getId(),
        'title' => $feed->getTitle(),
        'marketplace' => $feed->getType(),
        'status' => ($feed->getStatus()) ?  t('Enable'): t('Disable'),
        'operations' => implode(' | ', $operations),
      ),
    );
  }

  $content[] = array(
    '#type' => 'item',
    '#markup' => l(t('Add a feed'), 'admin/commerce/config/marketplaces/feed/add'),
  );

  // Create the page.
  $content['pager_table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no feed found in the database'),
  );

  // Attach the pager theme.
  $content['pager_pager'] = array('#theme' => 'pager');

  return $content;
}


/**
 * Returns the list of steps and their associated forms for the add or edit
 * feed form.
 *
 * @return array
 */
function _commerce_marketplaces_feeds_steps() {
  return array(
    1 => array('form' => 'commerce_marketplaces_feeds_feed_info'),
    2 => array('form' => 'commerce_marketplaces_feeds_feed_product_type'),
  );
}

/**
 * Add or edit form wrapper.
 *
 * This wrapper is a multisteps form to manage the configuration of feeds.
 *
 * This form has two defined submit handlers to process the different steps:
 *  - Previous: handles the way to get back one step.
 *  - Next:     handles each step form submission.
 */
function commerce_marketplaces_feed_settings_form_wrapper($form, &$form_state, $feed) {
  // Initialize a description of the steps for the wizard.
  if (empty($form_state['step'])) {
    $form_state['step'] = 1;
    $form_state['step_information'] = _commerce_marketplaces_feeds_steps();
  }
  $step = &$form_state['step'];

  // Call the function named in $form_state['step_information'] to get the
  // form elements to display for this step.
  $form = $form_state['step_information'][$step]['form']($form, $form_state, $feed);

  unset($form['actions']);

  // Show the 'previous' button if appropriate.
  if ($step > 1) {
    $form['prev'] = array(
      '#type' => 'submit',
      '#value' => t('Previous'),
      '#name' => 'prev',
      '#submit' => array('commerce_marketplaces_feed_settings_form_previous_submit'),
      '#limit_validation_errors' => array(),
    );
  }

  // Show the Next button only if there are more steps defined.
  if ($step < count($form_state['step_information'])) {
    // The Next button should be included on every step
    $form['next'] = array(
      '#type' => 'submit',
      '#value' => t('Next'),
      '#name' => 'next',
      '#submit' => array('commerce_marketplaces_feed_settings_form_next_submit'),
    );
  }
  else {
    $form['finish'] = array(
      '#type' => 'submit',
      '#value' => t('Save and start the mapping'),
    );
  }

  // Include each validation function defined for the different steps.
  if (function_exists($form_state['step_information'][$step]['form'] . '_validate')) {
    $form['next']['#validate'] = array($form_state['step_information'][$step]['form'] . '_validate');
  }

  if (function_exists($form_state['step_information'][$step]['form'] . '_submit')) {
    $form['next']['#validate'] = array($form_state['step_information'][$step]['form'] . '_submit');
  }

  return $form;
}

/**
 * Submit handler for the "previous" button.
 * - Stores away $form_state['values']
 * - Decrements the step counter
 * - Replaces $form_state['values'] with the values from the previous state.
 * - Forces form rebuild.
 *
 * You are not required to change this function.
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feed_settings_form_previous_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];
  if ($current_step > 1) {
    $current_step--;
    $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Submit handler for the 'next' button.
 * - Saves away $form_state['values']
 * - Increments the step count.
 * - Replace $form_state['values'] from the last time we were at this page
 *   or with array() if we haven't been here before.
 * - Force form rebuild.
 *
 * You are not required to change this function.
 *
 * @param $form
 * @param $form_state
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feed_settings_form_next_submit($form, &$form_state) {
  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  if ($current_step < count($form_state['step_information'])) {
    $current_step++;
    if (!empty($form_state['step_information'][$current_step]['stored_values'])) {
      $form_state['values'] = $form_state['step_information'][$current_step]['stored_values'];
    }
    else {
      $form_state['values'] = array();
    }
    $form_state['rebuild'] = TRUE;  // Force rebuild with next step.
    return;
  }
}

/**
 * Returns form elements for the 'personal info' page of the wizard. This is the
 * first step of the wizard, asking for two textfields: first name and last
 * name.
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feeds_feed_info($form, &$form_state, &$feed) {
  $form = array();
  $marketplaces = commerce_marketplaces_marketplace_load_all();

  if (empty($marketplaces)) {
    return $form = array(
      '#type' => 'markup',
      '#markup' => t('You must have a marketplace enable to add a feed')
    );
  }

  foreach ($marketplaces as $marketplace) {
    $marketplaces_option_list[$marketplace->getName()] = $marketplace->getTitle();
  }

  $form_state['feed'] = $feed;
  if (empty($feed)) {
    drupal_goto('admin/commerce/config/marketplaces/feeds/add');
  }

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($feed->title) ? $feed->getTitle() : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#required' => TRUE,
    '#machine_name' => array(
      'exists' => 'commerce_marketplaces_feed_load_by_name',
      'source' => array('title'),
    ),
    '#disabled' => empty($feed->name) ? FALSE : TRUE,
  );

  // TODO do not display this if the number of MP is equal to 1.
  // Defines the allowed number of instalments.
  $form['marketplace'] = array(
    '#type' => 'select',
    '#title' => t('Marketplace'),
    '#options' => $marketplaces_option_list,
    '#description' => t('TODO.'),
    '#default_value' => $feed->type,
    '#disabled' => empty($feed->type) ? FALSE : TRUE,
    '#required' => TRUE,
  );

  $form['frequency'] = array(
    '#type' => 'select',
    '#title' => t('Frequency export'),
    '#description' => t('TODO'),
    '#options' => array('1' => t('every hours'), '2' => t('every 2 hours'), '3' => t('every 4 hours')),
  );

  // Defines the allowed number of instalments.
  $form['status'] = array(
    '#type' => 'select',
    '#title' => t('Status'),
    '#options' => array(0 => t('Disable'), 1 => t('Enable')),
    '#description' => t('TODO.'),
    '#default_value' => $feed->status,
    '#required' => TRUE,
  );

  $form['#prefix'] = '<div id="marketplace-feed-settings">';
  $form['#suffix'] = '</div>';

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces')
  );
  return $form;
}

function commerce_marketplaces_feeds_feed_info_validate($form, &$form_state) {
  // TODO checker la validité de la marketpalce.
}

function commerce_marketplaces_feeds_feed_info_submit($form, &$form_state) {
  $marketplace = commerce_marketplaces_marketplace_load_by_name($form_state['values']['marketplace']);
  $form_state['marketplace'] = $marketplace;

  $feed = &$form_state['feed'];
  $feed->type = $marketplace->getName();
  $feed->name = $form_state['values']['name'];
  $feed->title = $form_state['values']['title'];
  $feed->frequency = $form_state['values']['frequency'];
  $feed->status = $form_state['values']['status'];
}


/**
 * Returns form elements for the 'location info' page of the wizard. This is the
 * second step of the wizard. This step asks for a textfield value: a City. This
 * step also includes a validation declared later.
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feeds_feed_product_type($form, &$form_state, &$feed) {
  $form = array();
  $marketplace = commerce_marketplaces_marketplace_load_by_name($feed->type);
  $form_state['marketplace'] = $marketplace;

  //$marketplace->getProductTypes();
  if (empty($feed)) {
    drupal_goto('admin/commerce/config/marketplaces/feeds/add');
  }

  $form['source_product_types'] = array(
    '#type' => 'select',
    '#title' => t('DC products types'),
    '#options' => commerce_product_type_get_name(),
    '#description' => t('TODO.'),
      '#default_value' => $feed->source_product_types,
    //'#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['target_product_types'] = array(
    '#type' => 'select',
    '#title' => t('Marketplace products types'),
    '#options' => $marketplace->getProductTypes(),
    '#description' => t('TODO.'),
    '#default_value' => $feed->target_product_types,
    //'#multiple' => TRUE,
    '#required' => TRUE,
  );

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces')
  );

  return $form;
}

/**
 * Wizard form submit handler.
 * - Saves away $form_state['values']
 * - Process all the form values.
 *
 * This demonstration handler just do a drupal_set_message() with the information
 * collected on each different step of the wizard.
 *
 * @param $form
 * @param $form_state
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feed_settings_form_wrapper_submit($form, &$form_state) {

  $current_step = &$form_state['step'];
  $form_state['step_information'][$current_step]['stored_values'] = $form_state['values'];

  dsm(func_get_args());
  dsm(__FUNCTION__);
  $feed = &$form_state['feed'];
  $marketplace = $form_state['marketplace'];
  dsm($form_state['values']);

  // In case of a multi select form.
  //$feed->source_product_types = array_keys($form_state['values']['source_product_types']);
  //$feed->target_product_types = array_keys($form_state['values']['target_product_types']);
  $feed->source_product_types = array($form_state['values']['source_product_types']);
  $feed->target_product_types = array($form_state['values']['target_product_types']);
  commerce_marketplaces_feed_save($feed);

  $form_state['redirect'] = url('admin/commerce/config/marketplaces/feed/' . $feed->id . '/mapping');
}



/**
 * Returns form elements for the 'other info' page of the wizard. This is the
 * thid and last step of the example wizard.
 *
 * @ingroup form_example
 */
function commerce_marketplaces_feeds_feed_fields_mapping($feed) {

  if (!$feed) {
    return FALSE;
  }

  dsm($feed, '$feed');


  // TODO : A supprimer. Vérifier que l'on a toujours bien un tableau en arrivé.
  if (!$feed->fields_mapping) {
    $feed->fields_mapping = array();
  }

  $form = array();

  // Load the marketplace properties
  $marketplace = commerce_marketplaces_marketplace_load($feed->type);

  $mapped_fields = $feed->getTargetProductTypeMappedFields(TRUE);

  // Displays already mapped fields.
  $form['mapped_fields'] = drupal_get_form('commerce_marketplaces_feed_mapping_fields_form', $feed);

  // Get the list of available fields for this Drupal Commerce product.
  $source_product_type_fields = commerce_marketplaces_feed_get_source_fields_mapping($feed->source_product_types);
  // Generate a field_id => field_label pair array of source fields.
  $source_product_type_optional_fields_widget_options = array();
  foreach ($source_product_type_fields as $field) {
    $source_product_type_optional_fields_widget_options[$field['field_id']] = $field['label'];
  }
  // Generate a dedicated form for all required makerplace fields.
  $form['required_fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Required fields'),
  );
  $target_product_type_required_fields = array_diff_key($feed->getTargetProductTypeFields(array('required' => TRUE)), $mapped_fields);

  foreach($target_product_type_required_fields as $field_name => $field_properties) {
    // Generate a add form for each required field. This form doesn't exist,
    // but is managed by hook_forms in order to give a unique form_id for
    // each form to generate.
    $form['required_fields'][$field_name] = drupal_get_form('commerce_marketplaces_feed_add_field_form_'. $field_name, $feed, array(), $source_product_type_optional_fields_widget_options, $field_properties);
  }

  // Generate a form to add non required field to the mapping.
  $target_product_type_optional_fields = array_diff_key($feed->getTargetProductTypeFields(array('required' => FALSE)), $mapped_fields);

  // Generate a field_id => field_label pair array of source fields.
  $target_product_type_optional_fields_widget_options = array();
  $target_fields_mapping = $feed->fieldsMapping();
  foreach ($target_product_type_optional_fields as $field) {
    $target_product_type_optional_fields_widget_options[$field[$target_fields_mapping['name']]] = $field[$target_fields_mapping['label']];
  }
  $form['fields'] = array(
    '#type' => 'fieldset',
    '#title' => t('Fields'),
  );
  $form['fields']['add'] = drupal_get_form('commerce_marketplaces_feed_add_field_form', $feed, $target_product_type_optional_fields_widget_options, $source_product_type_optional_fields_widget_options);


  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces/feeds')
  );
  return $form;
}

/**
 * @param $form
 * @param $form_state
 * @param $feed_fields_mapping
 * @param $source_product_type_fields
 * @param $target_product_type_fields
 * @return array
 */
function commerce_marketplaces_feed_mapping_fields_form($form, &$form_state, $feed) {
  // Generate the listing of already mapped fields.
  $header = array(
    t('Source'),
    t('Target'),
    t('Required'),
    '&nbsp;',
  );

  $form = array();
  $form_state['feed'] = $feed;

  $form['#mappings'] = array();
  $form['remove_flags'] = array('#tree' => TRUE);

  $rows = array();
  foreach ($feed->fields_mapping as $mapping) {

    $source = $mapping['source'];
    if (isset($source['field'])) {
      // TODO Verify if the field is still existing
      $field_source = t('Field : @field', array('@field' => $source['field']));
    }
    elseif (isset($source['value'])) {
      $field_source = t('Default value : @value', array('@value' => $source['value']));
    }
    // TODO Verify if the field is still existing
    $target = $mapping['target']['field'];

    if (isset($target)) {
      $form['#mappings'][$feed->getTargetProductTypeFieldProperty($target, 'name')] = array(
        'field_source' => $field_source,
        'field_target' => $feed->getTargetProductTypeFieldProperty($target, 'label'),
        'field_required' => $feed->getTargetProductTypeFieldProperty($target, 'required'),
      );
      $form['remove_flags'][$feed->getTargetProductTypeFieldProperty($target, 'name')] = array(
        '#type' => 'checkbox',
        '#title' => t('Remove'),
        '#prefix' => '<div class="commerce-marketpalce-remove">',
        '#suffix' => '</div>',
      );
    }
  }

  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
    //'#validate' => array('commerce_marketplaces_feeds_feed_remove_field_mapping_validate'),
    '#submit' => array('commerce_marketplaces_feeds_feed_remove_field_mapping_submit'),
  );
  return $form;
}



function commerce_marketplaces_feeds_feed_remove_field_mapping_submit($form, &$form_state) {
  //form_state_values_clean($form_state);
  $removed_fields = array();
  $feed = $form_state['feed'];

  $target_product_type_fields = $feed->getTargetProductTypeFields();

  // Marks fields that doesn't exist in the product type fields definition.
  // This could happens if the marketplace changed the definition of fields.
  $removed_fields += array_diff_key($feed->fields_mapping, $target_product_type_fields);

  // Removed selected fields by the user.
  $removed_fields += array_filter($form_state['values']['remove_flags']);

  foreach ($removed_fields as $key => $value) {
    unset($feed->fields_mapping[$key]);
  }
  commerce_marketplaces_feed_save($feed);
}



/**
 * @param $variables
 */
function theme_commerce_marketplaces_feed_mapping_fields_form($variables) {
  $form = $variables['form'];

  // Build the actual mapping table.
  $header = array(
    t('Source'),
    t('Target'),
    t('Required'),
    '&nbsp;',
  );
  $rows = array();

  foreach ($form['#mappings'] as $i => $mapping) {
    $rows[] = array(
      'data' => array(
        check_plain($mapping['field_source']),
        check_plain($mapping['field_target']),
        $mapping['field_required'] == TRUE ? t('Yes') : t('No'),
        drupal_render($form['remove_flags'][$i]),
      ),
    );
  }

//  if (!count($rows)) {
//    $rows[] = array(
//      array(
//        'colspan' => 4,
//        'data' => t('No mappings defined.'),
//      ),
//    );
//  }

  $output = theme('table', array('header' => $header, 'rows' => $rows, 'empty' => t('There are no marketplaces found'), 'attributes' => array('id' => 'commerce-marketplace-mapping-overview')));
  $output .= drupal_render_children($form);
  return $output;
}

/**
 * @param $form
 * @param $form_state
 * @param $feed
 * @param $target_field
 * @param array $source_product_type_fields_widget_options
 * @param array $target_product_type_fields_widget_options
 * @return mixed
 */
function commerce_marketplaces_feed_add_field_form($form, &$form_state, $feed, $target_product_type_fields_widget_options = array(), $source_product_type_fields_widget_options = array(), $target_field = array()) {
  $form_state['feed'] = $feed;

  $target_fields_mapping = $feed->fieldsMapping();

  $form['target'] = array(
    '#type' => 'select',
    '#default_value' => !empty($target_field)
      ? $target_field[$target_fields_mapping['name']]
      : '',
    '#description' => !empty($target_field['description'])
      ? t('@description', array('@description' => $target_field['description']))
      : '',
    '#options' => empty($target_field)
      ? array('' => t('Select a target field')) + $target_product_type_fields_widget_options
      : array($target_field[$target_fields_mapping['name']] => $target_field[$target_fields_mapping['label']]),
    '#disabled' => empty($target_field) ? FALSE : TRUE,
  );
  $form['select'] = array(
    '#type' => 'select',
    '#title' => t('Select a source field'),
    '#description' => t('DC'),
    '#options' => array('' => t('Select a source')) + $source_product_type_fields_widget_options,
  );
  $form ['textfield'] = array(
    '#type' => 'textfield',
    '#title' => t('Add a default value'),
    '#description' => t('DC'),
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Add'),
    '#validate' => array('commerce_marketplaces_feeds_feed_add_field_mapping_validate'),
    '#submit' => array('commerce_marketplaces_feeds_feed_add_field_mapping_submit'),
  );

  return $form;
}

function commerce_marketplaces_feeds_feed_add_field_mapping_validate($form, &$form_state) {

//  dsm(func_get_args());
  dsm( $form_state['values']);

  // TO DO Add control
  // le select ou le textfield doivent etre renseigné!
//  if (!$form_state['values']['target']) {
//    form_set_error('commerce_marketplaces_feed_add_field_form][target', t('You must select a target field to map with it'));
//  }
//  if (!$form_state['values']['select'] && !$form_state['values']['textfield']) {
//    form_set_error('commerce_marketplaces_feed_add_field_form][select', t('You must select a source field or filled in a default value'));
//    form_set_error('commerce_marketplaces_feed_add_field_form][textfield', '');
//  }

//  form_set_error(
//    implode('][', array_merge($form_parents, array('phone_number'))),
//    t('Eight or more same numbers are not allowed in cellphone number.')
//  );
}

function commerce_marketplaces_feeds_feed_add_field_mapping_submit($form, &$form_state) {
  $feed = $form_state['feed'];
  dsm(func_get_args());
  if ($feed) {
    // Prefer the value selected than the value filled out in the textfield.
    if (!empty($form_state['values']['select'])) {
      $feed->fields_mapping[$form_state['values']['target']] = array('source' => array('field' => $form_state['values']['select']), 'target' => array('field' => $feed->getSourceProductTypeFields($form_state['values']['target'])));
    }
    else{
      $feed->fields_mapping[$form_state['values']['target']] = array('source' => array('value' => $form_state['values']['textfield']), 'target' => array('field' => $feed->getSourceProductTypeFields($form_state['values']['target'])));
    }
  }
  commerce_marketplaces_feed_save($feed);
}


