<?php

/**
 * Form for listing all the marketplaces available.
 *
 */
function commerce_marketplaces_overview() {
  $content = array();
  $header = array(
    t('Title'),
    t('Description'),
    t('Status'),
    '&nbsp;',
  );

  $marketplaces = commerce_marketplaces_marketplace_load_all();

  $rows = array();
  foreach ($marketplaces as $marketplace) {

    $operations[] = l(t('Edit'), 'admin/commerce/config/marketplaces/marketplace/' . $marketplace->getId() . '/edit');
    $operations[] = l(t('Delete'), 'admin/commerce/config/marketplaces/marketplace/' . $marketplace->getId() . '/delete');
    $operations[] = $marketplace->isConfigurable() ? l(t('Configure'), 'admin/commerce/config/marketplaces/marketplace/' . $marketplace->getId() . '/configure') : '';

    $rows[] = array(
      'data' => array(
        'title' => $marketplace->getTitle(),
        'marketplace' => $marketplace->getDescription(),
        'status' => $marketplace->getStatus(),
        'operations' => implode(' | ', $operations),
      ),
    );
  }

  $plugins = commerce_marketplaces_get_marketplace_plugins();
  if (!empty($plugins)) {
    $content[] = array(
      '#type' => 'item',
      '#markup' => l(t('Add a Marketplace'), 'admin/commerce/config/marketplaces/marketplace/add'),
    );
  }

  // Create the page.
  $content['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#empty' => t('There are no marketplaces found'),
  );

  return $content;
}

function commerce_marketplaces_marketplace_settings_form($form, &$form_state, $marketplace = NULL) {
    // Add the breadcrumb for the form's location.
  commerce_marketplaces_set_breadcrumb();

  $form = array();
  $form_state['marketplace'] = $marketplace;

  $form['title'] = array(
    '#type' => 'textfield',
    '#title' => t('Title'),
    '#default_value' => isset($marketplace->title) ? $marketplace->getTitle() : '',
    '#maxlength' => 255,
    '#required' => TRUE,
  );

  $form['name'] = array(
    '#type' => 'machine_name',
    '#required' => FALSE,
    '#machine_name' => array(
      'exists' => 'commerce_marketplaces_marketplace_load',
      'source' => array('title'),
    ),
    '#disabled' => isset($marketplace->name) ? TRUE : FALSE,
  );

  $marketplaces_option_list = array();
  $plugins = commerce_marketplaces_get_marketplace_plugins();
    foreach($plugins as $plugin) {
    $marketplaces_option_list[$plugin['name']] = check_plain($plugin['title']);
  }
  $form['type'] = array(
    '#type' => 'select',
    '#title' => t('Marketplace'),
    '#options' => $marketplaces_option_list,
    '#description' => t('Select the Marketplace.'),
    '#default_value' => empty($marketplace->type) ? '' : $marketplace->type,
    '#disabled' => empty($marketplace->type) ? FALSE : TRUE,
    '#required' => TRUE,
  );

  $form['#prefix'] = '<div id="marketplace-settings">';
  $form['#suffix'] = '</div>';

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save and configure')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces')
  );

  return $form;
}


function commerce_marketplaces_marketplace_settings_form_validate($form, &$form_state) {}

function commerce_marketplaces_marketplace_settings_form_submit($form, &$form_state) {

  $marketplace = $form_state['marketplace'];
  if (empty($marketplace)) {
    $values = array(
      'name' => $form_state['values']['name'],
      'title' => $form_state['values']['title'],
      'type' => $form_state['values']['type'],
    );
    $merketplace = commerce_marketplaces_marketplace_create($values);
    $merketplace->save();
  }
}

/**
 * Form callback: edit a marketplace settings.
 */
function commerce_marketplaces_marketplace_configure_form($form, &$form_state, $marketplace = NULL) {
  if ($marketplace->isConfigurable() != TRUE) {
    drupal_set_message(t('The @name marketplace doesn\'t need to be configured', array('@name' => $marketplace->getTitle())));
    drupal_goto('admin/commerce/config/marketplaces');
  }

  // Add the breadcrumb for the form's location.
  commerce_marketplaces_set_breadcrumb();

  $form_state['marketplace'] = $marketplace;
  $form = array();
  if (method_exists($marketplace, 'configForm')) {
    $form = $marketplace->configForm($form, $form_state, $marketplace);
  }

  $form['#prefix'] = '<div id="marketplace-configure">';
  $form['#suffix'] = '</div>';

  $form['actions']['#type'] = 'actions';
  $form['actions']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save configuration')
  );
  $form['actions']['cancel'] = array(
    '#type' => 'markup',
    '#markup' => l(t('Cancel'), 'admin/commerce/config/marketplaces')
  );
  return $form;
}

/**s
 * Form callback validate: edit a marketplace settings.
 */
function commerce_marketplaces_marketplace_configure_form_validate($form, &$form_state) {
  $marketplace = $form_state['marketplace'];
  if (method_exists($marketplace, 'configFormValidate')) {
    $form = $marketplace->configFormValidate($form, $form_state, $marketplace);
  }
}

/**
 * Form callback submit: edit a marketplace settings.
 */
function commerce_marketplaces_marketplace_configure_form_submit($form, &$form_state) {
  $marketplace = $form_state['marketplace'];
  if (method_exists($marketplace, 'configFormSubmit')) {
    $marketplace->configFormSubmit($form, $form_state, $marketplace);
  }
  form_state_values_clean($form_state);
  foreach ($form_state['values'] as $key => $value) {
    $marketplace->setConfig($key, $value);
  }
  $marketplace->save();

  $form_state['redirect'] = array('admin/commerce/config/marketplaces/' . $marketplace->getId() );
}


/**
 * Sets the breadcrumb for administrative trackers pages.
 */
function commerce_marketplaces_set_breadcrumb() {
  $breadcrumb = array(
    l(t('Home'), '<front>'),
    l(t('Administration'), 'admin'),
    l(t('Store settings'), 'admin/commerce/config'),
    l(t('Marketplaces'), 'admin/commerce/config/marketplaces'),
  );
  drupal_set_breadcrumb($breadcrumb);
}

